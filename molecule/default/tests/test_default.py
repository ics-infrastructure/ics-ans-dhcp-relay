import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('dhcp_relay')


def test_dhcrelay(host):
    service = host.service("dhcrelay")
    assert service.is_running
    assert service.is_enabled
